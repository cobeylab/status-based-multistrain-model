## ----Prep, Prelims, and Packages----------------------------------
require(ggplot2)
require(plyr)
require(reshape2)
require(magrittr)
theme_set(theme_bw())
require(pomp)
stopifnot(packageVersion("pomp")>="0.66-2")
options(
  keep.source=TRUE,
  stringsAsFactors=FALSE,
  encoding="UTF-8",
  scipen=5,
  cores=5
  )

require(foreach)
require(doMC)
registerDoMC()

#want to set a fixed random seed to help with replicability and debugging
save.seed <- .Random.seed
set.seed(334388458L,kind="L'Ecuyer")

## ----Setting plotting to pdf specifics-----------------------------
#name plots something informative

pdf("SEIR_seasonal-inf1_new-SD-medium.pdf", width = 11, height = 8.5)


###############################################################################
## ----building custom pomp object for SEIR model-----------------------------#
###############################################################################

## ---------True Paramters, Data and Covariates---------------------------------------------------

#start out with a vector of named param values and corresponding true values
params_true = read.csv(text="
R0,mu,sigma,gamma,epsilon,omega,rho,psi,sigmaSE
1.6,3.00e-01,7.20e+01,6.6e+01,0.8e-1,1.0e+0,0.9e-01,1.0e-01,1.0e-2
",stringsAsFactors=FALSE)

params_true = unlist(params_true)

#set up covariates for monthly data
year_min = 1939.00
year_max = 1964.00
pop_ave = 3000000	#set high to reduce stochasticity
birth_ave = 50		#not actually used in code yet

time = seq(from=year_min,to=year_max,by=1/12)  #setting up time vector to make sure all of the covariates line up correctly
number=length(time)

#packaged time and all covariates in a single data.frame to be stored in pomp object later
covar = data.frame(
	time,
	pop = rep(pop_ave, length.out = number),
	birthrate = rep(birth_ave, length.out = number))

#prep dummy dataset to be replaced with a simulated dataset below
year_min = 1950.00
year_max = 1964.00
case_ave = 11.0		#just a dummy value now.  Will be replaced with simulation data after running it

time = seq(from=year_min,to=year_max,by=1/24)  #setting up time vector to make sure all of the covariates line up correctly
number=length(time)

#package data and corresponding time vector in a data.frame to be added to pomp object later
dat = data.frame(
	time,
	cases = rep(case_ave, length.out = number))


## ----rprocess--------------We require a simulator for this model. The following builds a simulator.-----------------------------------------
rproc <- Csnippet("
  double beta, br, seas, foi, dw, births; 	//Need to declare any local objects (i.e. things not already in pomp object m1)
  double rate[6], trans[6];
 
//---------Seasonal forcing in infection rate (sinusoidal curve)----------------
  t = (t-floor(t))*365.25;
  seas = (1.0+epsilon*cos( 2.0*PI*( t + 365.25*(0.750) )/(omega*365.25) )); 
  //seas = (1.0+epsilon*cos( 2.0*PI*( t + 12.0*(phase+0.5) )/12.0 ); 	//original function from Sarah's code
  //seas = 1.0;		//if you want to remove seasonality for testing purposes

//--------- transmission rate, calculated from R0----------------
  beta = R0*(gamma+mu)*seas;	//place to add seasonality

//--------- expected force of infection-----------------------
  foi = beta*I/pop;		//CK// 	FORCE OF INFECTION
  //foi = beta*pow(I+iota,alpha)/pop;		//equation 2.1 from He et al. 2016

//--------- white noise (extrademographic stochasticity)-----------------
  dw = rgammawn(sigmaSE,dt);

//--------- transition rates between compartments-----------------
  rate[0] = foi*dw/dt;  // stochastic force of infection              
  //rate[0] = foi;  // deterministic force of infection
  rate[1] = mu;			    // natural S death
  rate[2] = sigma;		  // rate of ending of latent stage
  rate[3] = mu;			    // natural E death
  rate[4] = gamma;		  // recovery
  rate[5] = mu;			    // natural I death

//--------- birth rate----------------
  // constant births
  br = mu*pop;
  //births = mu*dt;
  // Poisson births
  births = rpois(br*dt);
 
//---------transitions between classes----------------
  reulermultinom(2,S,&rate[0],dt,&trans[0]);
  reulermultinom(2,E,&rate[2],dt,&trans[2]);
  reulermultinom(2,I,&rate[4],dt,&trans[4]);

  S += births   - trans[0] - trans[1];
  E += trans[0] - trans[2] - trans[3];
  I += trans[2] - trans[4] - trans[5];
  R = pop - S - E - I;
  W += (dw - dt)/sigmaSE;  // standardized i.i.d. white noise
  C += trans[4];           // true incidence
")

## ----initializer-------------The following codes assume that the fraction of the population in each of the four compartments is known.--------------------------------------------
#currently set to produce nice damped-cycles
#original code estimated S_0, E_0, I_0, and R_0 from the data

initlz <- Csnippet("
  double S_0=0.70;
  double E_0=0.0;
  double I_0=0.0;
  double R_0=0.30;

  double m = pop/(S_0+E_0+I_0+R_0);
  S = nearbyint(m*S_0)-100;
  E = nearbyint(m*E_0);
  I = nearbyint(m*I_0)+100;
  R = nearbyint(m*R_0);
  W = 0;
  C = 0;
")

## ----dmeasure----------overdispersed binomial measurement model. --------------------------------------------------
#Actually calculates the likelihood
#Includes both under-reporting (rho) and measurement error (psi)

dmeas <- Csnippet("
  double m = rho*C;
  double v = m*(1.0-rho+psi*psi*m);
  double tol = 1.0e-18;
  if (cases > 0.0) {
	  lik = pnorm(cases+0.5,m,sqrt(v)+tol,1,0)-pnorm(cases-0.5,m,sqrt(v)+tol,1,0)+tol;
  } else {
    lik = pnorm(cases+0.5,m,sqrt(v)+tol,1,0)+tol;
  }
")

## ----rmeasure-----------The following code stochastically simulates cases|C.-------------------------------------------------
#Observation model

rmeas <- Csnippet("
  double m = rho*C;
  double v = m*(1.0-rho+psi*psi*m);
  double tol = 1.0e-18;
  cases = rnorm(m,sqrt(v)+tol);
  if (cases > 0.0) {
    cases = nearbyint(cases);
  } else {
    cases = 0.0;
  }
")

## --------transformations----------------------------------------------------------
#pomp produces random number from -inf to +inf.  Therefore, we need to include transformations if we want to limit the search area for our paramters
#"log" ensures the parameter never takes a zero or negative value
#"logit" binds the parameter between 0 and 1, uninclusive
#when adding or removing parameters, be sure to add them to the "toEst" and "fromEst" vectors

toEst <- Csnippet("
  Tmu = log(mu);		// mu = natural death rate
  Tsigma = log(sigma);		// sigma = rate of ending of latent stage
  Tgamma = log(gamma);		// gamma = recovery
//  Talpha = log(alpha);		// alpha = mixing parameter
//  Tiota = log(iota);		// iota = mean number of infectious individuals visiting the population
  Trho = logit(rho);		// rho = rate of under-reporting 	
//  Tcohort = logit(cohort);		//cohort = cohort effect from measles
  Tepsilon = logit(epsilon);	//epsilon = amplitude of seasonal forcing
  Tomega = log(omega);		//omega = period of seasonal forcing
//  Tamplitude = logit(amplitude);	//amplitude = amplitude of seasonal forcing
  TsigmaSE = log(sigmaSE);		// sigmaSE = standard error for white noise (extrademographic stochasticity)
  Tpsi = log(psi);		// psi = rate of overdispersion
  TR0 = log(R0);		// R0 = R_0 = beta/(gamma+mu); the basic reproductive number
//  to_log_barycentric (&TS_0, &S_0, 4);
")

#Functions to back-transform the parameters
#be sure to include paramters in both functions

fromEst <- Csnippet("
  Tmu = exp(mu);
  Tsigma = exp(sigma);
  Tgamma = exp(gamma);
//  Talpha = exp(alpha);
//  Tiota = exp(iota);
  Trho = expit(rho);
//  Tcohort = expit(cohort);
  Tepsilon = expit(epsilon);	
  Tomega = exp(omega);	
//  Tamplitude = expit(amplitude);
  TsigmaSE = exp(sigmaSE);
  Tpsi = exp(psi);
  TR0 = exp(R0);
//  from_log_barycentric (&TS_0, &S_0, 4);
")



## ----pomp-construction (FIRST ROUND!!)---------Putting all the pieces together to build the pomp object------------------------------------------
## m1 is the pomp object that contains both the models, the data, the covariates, everything

#at this point, we want to assemble the pomp object with a dummy data set.
#once the object is ready, we will use it to produce simulated data
#we will then replace the dummy data with the simulated time-series

#when adding or removing parameters, be sure to add them to the "paramnames" vector below
dat %>% 
  pomp(toEstimationScale=toEst,
       fromEstimationScale=fromEst,
       t0=with(dat,2*time[1]-time[2]),
       time="time",
       rprocess=euler.sim(rproc,delta.t=1/365.25),
       initializer=initlz,
       dmeasure=dmeas,
       rmeasure=rmeas,
       covar=covar,
       tcovar="time",
       zeronames=c("C"),
       statenames=c("S","E","I","R","C","W"),
       paramnames=c("R0","mu","sigma","gamma","epsilon","omega",
                    "rho","psi","sigmaSE")
       ) -> m1


## ----Simulate the model using the pomp object, m1-----------------------------#

sim_out = simulate(m1, params=params_true,nsim=1,as.data.frame=TRUE,include.data=TRUE)


## ----repackage simulation output as the dataset to which we will fit the params--------------------------------------------------

sim_out.sim= sim_out[sim_out$sim != "data",]

dat = data.frame(
	time,
	cases = sim_out.sim$cases)

#-------rebuild pomp object with simulated data, not dummy data

dat %>% 
  pomp(toEstimationScale=toEst,
       fromEstimationScale=fromEst,
       t0=with(dat,2*time[1]-time[2]),
       time="time",
       rprocess=euler.sim(rproc,delta.t=1/365.25),
       initializer=initlz,
       dmeasure=dmeas,
       rmeasure=rmeas,
       covar=covar,
       tcovar="time",
       zeronames=c("C"),
       statenames=c("S","E","I","R","C","W"),
       paramnames=c("R0","mu","sigma","gamma","epsilon","omega",
                    "rho","psi","sigmaSE")
       ) -> m1


## -----Plotting simulations of the stochastic model--------------------------------------------------
#Page 1: Simulations of stochastic base model.  The top-left panel is the run I used to estimate parameters.
#  The other 9 demonstrate the variability in the current model/parameters.

m1 %>% 
  simulate(params=params_true,nsim=9,as.data.frame=TRUE,include.data=TRUE) %>%
  ggplot(aes(x=time,y=cases,group=sim,color=(sim=="data")))+
  guides(color=FALSE)+
  geom_line()+facet_wrap(~sim,ncol=2)


#####################################################################################################
## ----pomp object is built; time to recover true parameters using mif2-----------------------------#
#####################################################################################################


## ----multi-mif2-eval,results='hide'-----------------------------
require(foreach)
require(doMC)
registerDoMC()

nmifs=60   #used to dictate how many mifs to perform in each of the four rounds in the cooling process. Base was 50
nparticles = 2000   #number of particles in the filtering process.  Base was 2000
nparticles2 = 10000  #number of particles used when estimating the likelihood values. Base was 10,000

save.seed <- .Random.seed
set.seed(334388458L,kind="L'Ecuyer")

#adding comments between lines 44 and 54 seems to break the code.  Not sure why...
#the line "mf <- foreach(i=1:5," dictates how many runs of mif2 to perform in parallel
estpars <- c("R0","mu","sigma","gamma","epsilon","omega","rho","psi","sigmaSE")
mf <- foreach(i=1:10,
              .inorder=FALSE,
              .options.multicore=list(set.seed=TRUE)
              ) %dopar%
  {
    theta.guess <- params_true
    theta.guess[estpars] <- rlnorm(
      n=length(estpars),
      meanlog=log(theta.guess[estpars]),
      sdlog=1.0
      )
    m2 <- mif2(
      m1,			#line that feeds the pomp object (i.e. the actual process and observation models)
      Nmif=nmifs,			#Nmif controls how many number of filtering iterations to perform. Originially set to 50.
      start=theta.guess,
      transform=TRUE,
      rw.sd=rw.sd(R0=0.01,mu=0.01,sigma=0.005,gamma=0.005,epsilon=.01,omega=.005,rho=0.02,psi=0.02,sigmaSE=0.01),  #dictating variance around proposals to search for each param. very important for tuning future code
      cooling.fraction.50=0.95,
      Np=nparticles			#Np controls the number of particles used in filtering.  Default set to 2000
      )
    m2 <- continue(m2,Nmif=nmifs,cooling.fraction=0.8)	#the function "continue" allows one to add iterations to a chain 
    m2 <- continue(m2,Nmif=nmifs,cooling.fraction=0.6)	#in the process, we perform 3 more sets of 50 steps
    m2 <- continue(m2,Nmif=20,cooling.fraction=0.2)	#for each set, we are decreasing the cooling.fraction (the manner in which the intensity of  the  parameter  perturbations  is  reduced  with  successive  filtering  iterations.)
							#reducing the cooling.fraction reduces the perterbations made to the parameters in each iteration
    ll <- replicate(n=10,logLik(pfilter(m2,Np=nparticles2)))		#Caluclating likelihood (dafault was 10000), but I'm a little confused by this step.  Why use pfilter? M2 contains the results from the previous iterations, so it must just be extrating something
    list(mif=m2,ll=ll)
    }

## -----calculating final stats for the lhood runs---------------------------
loglik.true <- replicate(n=10,logLik(pfilter(m1,Np=nparticles2,params=params_true)))   #Np dafault was 10,000. extracting the "true" loglikelhood, but I'm not exactly sure how the replicate function is doing this...  It might be repeating the whole optimization processes anew, but that seems inefficient
loglik.true <- logmeanexp(loglik.true,se=TRUE)
theta.mif <- t(sapply(mf,function(x)coef(x$mif)))	#extracting the best lhood value from the mf object we created above
loglik.mif <- t(sapply(mf,function(x)logmeanexp(x$ll,se=TRUE)))
best <- which.max(loglik.mif[,1])
theta.mif <- theta.mif[best,]
loglik.mif <- loglik.mif[best,]
rbind(
  mle=c(signif(theta.mif[estpars],9),loglik=round(loglik.mif,2)),
  truth=c(signif(params_true[estpars],9),loglik=round(loglik.true,2))
  ) -> results.table

## ----plotting the results of the parallel runs---------------------------
op <- par(mfrow=c(4,3),mar=c(3,3,2,0),mgp=c(2,1,0),bty='l')
loglik <- sapply(mf,function(x)conv.rec(x$mif,"loglik"))
log.R0 <- sapply(mf,function(x)conv.rec(x$mif,"R0"))
log.mu <- sapply(mf,function(x)conv.rec(x$mif,"mu"))
log.sigma <- sapply(mf,function(x)conv.rec(x$mif,"sigma"))
log.gamma <- sapply(mf,function(x)conv.rec(x$mif,"gamma"))
log.epsilon <- sapply(mf,function(x)conv.rec(x$mif,"epsilon"))
log.omega <- sapply(mf,function(x)conv.rec(x$mif,"omega"))
log.rho <- sapply(mf,function(x)conv.rec(x$mif,"rho"))
log.psi <- sapply(mf,function(x)conv.rec(x$mif,"psi"))
log.sigmaSE <- sapply(mf,function(x)conv.rec(x$mif,"sigmaSE"))
#log.S_0 <- sapply(mf,function(x)conv.rec(x$mif,"S_0"))
#log.E_0 <- sapply(mf,function(x)conv.rec(x$mif,"E_0"))
#log.I_0 <- sapply(mf,function(x)conv.rec(x$mif,"I_0"))
#log.R_0 <- sapply(mf,function(x)conv.rec(x$mif,"R_0"))

matplot(loglik,type='l',lty=1,xlab="",ylab=expression(log~L),xaxt='n',ylim=c((min(loglik,na.rm=T)*.1 + max(loglik,na.rm=T)), max(loglik,na.rm=T)+3))
#matplot(loglik,type='l',lty=1,xlab="",ylab=expression(log~L),xaxt='n',ylim=c(min(loglik,na.rm=T)-12, max(loglik,na.rm=T)+3))   #old range for mle plot
matplot(log.R0,type='l',lty=1,xlab="",ylab=expression(log~R0),xaxt='n')
matplot(log.mu,type='l',lty=1,xlab="",ylab=expression(log~mu),xaxt='n')
matplot(log.sigma,type='l',lty=1,xlab="",ylab=expression(log~sigma),xaxt='n')
matplot(log.gamma,type='l',lty=1,xlab="",ylab=expression(log~gamma),xaxt='n')
matplot(log.epsilon,type='l',lty=1,xlab="",ylab=expression(log~epsilon),xaxt='n')
matplot(log.omega,type='l',lty=1,xlab="",ylab=expression(log~omega),xaxt='n')
matplot(log.rho,type='l',lty=1,xlab="",ylab=expression(log~rho),xaxt='n')
matplot(log.psi,type='l',lty=1,xlab="",ylab=expression(log~psi),xaxt='n')
matplot(log.sigmaSE,type='l',lty=1,xlab="",ylab=expression(log~sigmaSE),xaxt='n')
#matplot(log.S_0,type='l',lty=1,xlab="",ylab=expression(log~S_0),xaxt='n')
#matplot(log.E_0,type='l',lty=1,xlab="",ylab=expression(log~E_0),xaxt='n')
#matplot(log.I_0,type='l',lty=1,xlab="",ylab=expression(log~I_0),xaxt='n')
#matplot(log.R_0,type='l',lty=1,xlab="",ylab=expression(log~R_0),xaxt='n')
par(op)

## ----first-mif-results-table,echo=FALSE,cache=FALSE----------------------
#prints table with true and estimated parameter values, as well as log likelihood values
print(results.table)


## ----plot of simulation of estimated parameter set-----------------------------------------
#performs 100 simulations using the estimated params to see how good the output is compared to original data

m1 %>% 
  simulate(params=theta.mif[estpars],nsim=100,as.data.frame=TRUE,include.data=TRUE) %>%
  subset(select=c(time,sim,cases)) %>%
  mutate(data=sim=="data") %>%
  ddply(~time+data,summarize,
        p=c(0.05,0.5,0.95),q=quantile(cases,prob=p,names=FALSE)) %>%
  mutate(p=mapvalues(p,from=c(0.05,0.5,0.95),to=c("lo","med","hi")),
         data=mapvalues(data,from=c(TRUE,FALSE),to=c("data","simulation"))) %>%
  dcast(time+data~p,value.var='q') %>%
  ggplot(aes(x=time,y=med,color=data,fill=data,ymin=lo,ymax=hi))+
  geom_ribbon(alpha=0.2)


dev.off()   	#close plotting to pdf

