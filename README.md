# README #

Building an R script that will be scalable for future pomp models using mif2.  The plan is to strip down the measles code from http://kingaa.github.io/sbied/measles/measles-profile.html into a basic pomp SEIR model structure.  This base code will then be hooked up to a simulation program that will test the code inside of the pomp object before trying to fit the model to the data (http://kingaa.github.io/pomp/vignettes/mif2.html).  To test the code, the algorithm simulates the model based on known initial parameters, then runs the optimization routine to recover the known parameters.  This process gives us an idea of the range of error inherent in estimating parameters for a stochastic mode.  This step is very important, because it will check the simulation and optimization code with known parameters and known data.  

### What is this repository for? ###

* Quick summary

Measles_pomp_SEIRbasic.R is the stripped down SEIR model.  It currently fits the model to a measles dataset.

Measles_pomp_SEIRsimulation.R is the simulation program that takes an SDE model in a pomp object, simulates it and measures the variance observed when recovering the known parameter set.


* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Need to have the R package pomp installed.  Follow instructions here: http://kingaa.github.io/sbied/prep/preparation.html

This is also an option, but the first one worked for me on a linux system: http://kingaa.github.io/pomp/install.html

After that, just run the whole code.  I'll add in appropriate .sbatch files for running the code in parallel on midway.rcc.

* Configuration
* Dependencies
* Database configuration

Measles_pomp_SEIRbasic.R uses data from AKing's website.

Measles_pomp_SEIRsimulation.R will simulate all of the necessary data and covariates within it's own code.

* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Contact Colin at ckyle@uchicago.edu

* Other community or team contact