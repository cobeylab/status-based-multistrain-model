## ----prelims,echo=FALSE,cache=FALSE--------------------------------------
require(ggplot2)
require(plyr)
require(reshape2)
require(magrittr)
theme_set(theme_bw())
require(pomp)
stopifnot(packageVersion("pomp")>="0.66-2")
options(
  keep.source=TRUE,
  stringsAsFactors=FALSE,
  encoding="UTF-8",
  scipen=5,
  cores=5
  )

## ----gompertz-init,cache=FALSE-------------------------------------------
#require(pomp)
#pompExample(gompertz)
#theta <- coef(gompertz)		#loading up true parameter values for K, r, sigma, tau, and X_0; important step
#theta.true <- theta

## ----gompertz-multi-mif2-eval,results='hide'-----------------------------
require(foreach)
require(doMC)
registerDoMC()

save.seed <- .Random.seed
set.seed(334388458L,kind="L'Ecuyer")

###############################################################################
## ----building custom pomp object for SEIR model-----------------------------#
###############################################################################

#start out with a vector of named param values

#paramnames <- c("R0","mu","sigma","gamma","rho","psi","S_0","E_0","I_0","R_0")

params_true = read.csv(text="
R0,mu,sigma,gamma,rho,psi,S_0,E_0,I_0,R_0
5.68e+01,2.00e-02,2.89e+01,3.04e+01,4.88e-01,1.16e-01,2.97e-02,5.17e-05,5.14e-05,9.70e-01
",stringsAsFactors=FALSE)

#params_true = read.csv(text="
#R0,mu,sigma,gamma,rho,psi,S_0,E_0,I_0,R_0
#5.68e+01,2.00e-02,2.89e+01,3.04e+01,4.88e-01,1.16e-01,2.97e-02,5.17e-05,5.14e-05,9.70e-01
#",stringsAsFactors=FALSE)

params_true = unlist(params_true)

#set up covariates for monthly data
year_min = 1939.00
year_max = 1964.00
pop_ave = 3000000
birth_ave = 50000

time = seq(from=year_min,to=year_max,by=1/12)  #setting up time vector to make sure all of the covariates line up correctly
number=length(time)

covar = data.frame(
	time,
	pop = rep(pop_ave, length.out = number),
	birthrate = rep(birth_ave, length.out = number))

#prep dummy dataset to be replaced with a simulated dataset below
year_min = 1950.00
year_max = 1964.00
case_ave = 511.0

time = seq(from=year_min,to=year_max,by=1/24)  #setting up time vector to make sure all of the covariates line up correctly
number=length(time)

dat = data.frame(
	time,
	cases = rep(case_ave, length.out = number))


## ----rprocess--------------We require a simulator for this model. The following builds a simulator.-----------------------------------------
rproc <- Csnippet("
  double beta, br, seas, foi, births;
//  double beta, br, seas, foi, dw, births;
  double rate[6], trans[6];
  
//   br = 50000;  //CK// REMOVING COHORT EFFECT IN EASY WAY TO COMPARE WITH FUTURE CODE 
//  br = birthrate;  //CK// REMOVING COHORT EFFECT IN EASY WAY TO COMPARE WITH FUTURE CODE 

  // term-time seasonality
//  t = (t-floor(t))*365.25;
//  if ((t>=7&&t<=100) || (t>=115&&t<=199) || (t>=252&&t<=300) || (t>=308&&t<=356))
//      seas = 1.0+amplitude*0.2411/0.7589;
//    else
//      seas = 1.0-amplitude;

   seas = 1.0;	//CK// REMOVING SEASONALITY FOR TESTING PURPOSES

  //--------- transmission rate----------------
  beta = R0*(gamma+mu);
//  beta = R0*(gamma+mu)*seas;	//place to add seasonality
  //--------- expected force of infection-----------------------
  foi = beta*I/pop;		//CK// 	FORCE OF INFECTION
//  foi = beta*(I+iota)/pop;		//CK// 	FORCE OF INFECTION
//  foi = beta*pow(I+iota,alpha)/pop;		//equation 2.1 from He et al. 2016
  //--------- white noise (extrademographic stochasticity)-----------------
//  dw = rgammawn(sigmaSE,dt);

  rate[0] = foi;  // deterministic force of infection
//  rate[0] = foi*dw/dt;  // stochastic force of infection              
  rate[1] = mu;			    // natural S death
  rate[2] = sigma;		  // rate of ending of latent stage
  rate[3] = mu;			    // natural E death
  rate[4] = gamma;		  // recovery
  rate[5] = mu;			    // natural I death

//--------- birth rate----------------
  // constant births
  br = mu*pop;
//  births = mu*dt;
//    // Poisson births
  births = rpois(br*dt);
  
  // transitions between classes
  reulermultinom(2,S,&rate[0],dt,&trans[0]);
  reulermultinom(2,E,&rate[2],dt,&trans[2]);
  reulermultinom(2,I,&rate[4],dt,&trans[4]);

  S += births   - trans[0] - trans[1];
  E += trans[0] - trans[2] - trans[3];
  I += trans[2] - trans[4] - trans[5];
  R = pop - S - E - I;
//  W += (dw - dt)/sigmaSE;  // standardized i.i.d. white noise
  C += trans[4];           // true incidence
")

## ----initializer-------------The following codes assume that the fraction of the population in each of the four compartments is known.--------------------------------------------
#initlz <- Csnippet("
#//  double m = pop/(S_0+E_0+I_0+R_0);
#  S = nearbyint(pop) - 1;
#  E = 0;
#  I = 1;
#  R = 0;
#  C = 0;
#")

initlz <- Csnippet("
  double m = pop/(S_0+E_0+I_0+R_0);
  S = nearbyint(m*S_0);
  E = nearbyint(m*E_0);
  I = nearbyint(m*I_0);
  R = nearbyint(m*R_0);
//  W = 0;
  C = 0;
")

## ----dmeasure----------overdispersed binomial measurement model. Includes both under-reporting and measurement error--------------------------------------------------
dmeas <- Csnippet("
  double m = rho*C;
  double v = m*(1.0-rho+psi*psi*m);
  double tol = 1.0e-18;
  if (cases > 0.0) {
	  lik = pnorm(cases+0.5,m,sqrt(v)+tol,1,0)-pnorm(cases-0.5,m,sqrt(v)+tol,1,0)+tol;
  } else {
    lik = pnorm(cases+0.5,m,sqrt(v)+tol,1,0)+tol;
  }
")

## ----rmeasure-----------The following code stochastically simulates cases|C.-------------------------------------------------
rmeas <- Csnippet("
  double m = rho*C;
  double v = m*(1.0-rho+psi*psi*m);
  double tol = 1.0e-18;
  cases = rnorm(m,sqrt(v)+tol);
  if (cases > 0.0) {
    cases = nearbyint(cases);
  } else {
    cases = 0.0;
  }
")


## ----transforms---------when adding or removing parameters, be sure to add them to the "toEst" and "fromEst" vectors-------------------------------------------------
toEst <- Csnippet("
  Tmu = log(mu);		// mu = natural death rate
  Tsigma = log(sigma);		// sigma = rate of ending of latent stage
  Tgamma = log(gamma);		// gamma = recovery
//  Talpha = log(alpha);		// alpha = mixing parameter
//  Tiota = log(iota);		// iota = mean number of infectious individuals visiting the population
  Trho = logit(rho);		// rho = rate of under-reporting 	
//  Tcohort = logit(cohort);		//cohort = cohort effect from measles
//  Tamplitude = logit(amplitude);	//amplitude = amplitude of seasonal forcing
//  TsigmaSE = log(sigmaSE);		// sigmaSE = standard error for white noise (extrademographic stochasticity)
  Tpsi = log(psi);		// psi = rate of overdispersion
  TR0 = log(R0);		// R0 = R_0 = beta/(gamma+mu); the basic reproductive number
  to_log_barycentric (&TS_0, &S_0, 4);
")

fromEst <- Csnippet("
  Tmu = exp(mu);
  Tsigma = exp(sigma);
  Tgamma = exp(gamma);
//  Talpha = exp(alpha);
//  Tiota = exp(iota);
  Trho = expit(rho);
//  Tcohort = expit(cohort);
//  Tamplitude = expit(amplitude);
//  TsigmaSE = exp(sigmaSE);
  Tpsi = exp(psi);
  TR0 = exp(R0);
  from_log_barycentric (&TS_0, &S_0, 4);
")



## ----pomp-construction---------Putting all the pieces together to build the pomp object------------------------------------------
##-m1 is the pomp object that contains both the models, the data, the covariates, everything
## - m1 is what we will want to rebuild for a basic SIR model, then feed it into the gompertz mif2 code for testing

#when adding or removing parameters, be sure to add them to the "paramnames" vector below
dat %>% 
  pomp(toEstimationScale=toEst,
       fromEstimationScale=fromEst,
       t0=with(dat,2*time[1]-time[2]),
       time="time",
       rprocess=euler.sim(rproc,delta.t=1/365.25),
       initializer=initlz,
       dmeasure=dmeas,
       rmeasure=rmeas,
       covar=covar,
       tcovar="time",
       zeronames=c("C"),
       statenames=c("S","E","I","R","C"),
       paramnames=c("R0","mu","sigma","gamma",
                    "rho","psi",
                    "S_0","E_0","I_0","R_0")
       ) -> m1


###############################################################################
## ----pomp object is built; time to build simulated data set-----------------------------#
###############################################################################

sim_out = simulate(m1, params=params_true,nsim=1,as.data.frame=TRUE,include.data=TRUE)


## ----plot initial run of model simulation--------------------------------------------------
  ggplot(sim_out, aes(x=time,y=cases,group=sim,color=(sim=="data")))+
  guides(color=FALSE)+
  geom_line()+facet_wrap(~sim,ncol=2)

#alternet plotting routine to look at 9 stochastic simulations of the model
#m1 %>% 
#  simulate(params=params_true,nsim=9,as.data.frame=TRUE,include.data=TRUE) %>%
#  ggplot(aes(x=time,y=cases,group=sim,color=(sim=="data")))+
#  guides(color=FALSE)+
#  geom_line()+facet_wrap(~sim,ncol=2)

## ----repackage simulation output as the dataset to which we will fit the params--------------------------------------------------

#prep dummy dataset to be replaced with a simulated dataset below
year_min = 1950.00
year_max = 1964.00
case_ave = 511.0

time = seq(from=year_min,to=year_max,by=1/24)  #setting up time vector to make sure all of the covariates line up correctly
number=length(time)

sim_out.sim= sim_out[sim_out$sim == 1,]

dat = data.frame(
	time,
	cases = sim_out.sim$C)

#rebuild pomp object with simulated data, not dummy data

dat %>% 
  pomp(toEstimationScale=toEst,
       fromEstimationScale=fromEst,
       t0=with(dat,2*time[1]-time[2]),
       time="time",
       rprocess=euler.sim(rproc,delta.t=1/365.25),
       initializer=initlz,
       dmeasure=dmeas,
       rmeasure=rmeas,
       covar=covar,
       tcovar="time",
       zeronames=c("C"),
       statenames=c("S","E","I","R","C"),
       paramnames=c("R0","mu","sigma","gamma",
                    "rho","psi",
                    "S_0","E_0","I_0","R_0")
       ) -> m1



## ----pfilter1------------------------------------------------------------
#require(foreach)
#require(doMC)

#registerDoMC()

#set.seed(998468235L,kind="L'Ecuyer")
#mcopts <- list(preschedule=FALSE,set.seed=TRUE)

#foreach(i=1:4,
#        .packages="pomp",
#        .options.multicore=mcopts) %dopar% {
#  pfilter(m1,Np=10000,params=params_true)
#} -> pfs
#logmeanexp(sapply(pfs,logLik),se=TRUE)


## ----multi-mif2-eval,results='hide'-----------------------------
require(foreach)
require(doMC)
registerDoMC()

save.seed <- .Random.seed
set.seed(334388458L,kind="L'Ecuyer")

#adding comments between lines 44 and 54 seems to break the code.  Not sure why...
estpars <- c("R0","mu","sigma","gamma","rho","psi","S_0","E_0","I_0","R_0")
mf <- foreach(i=1:5,
              .inorder=FALSE,
              .options.multicore=list(set.seed=TRUE)
              ) %dopar%
  {
    theta.guess <- params_true
    theta.guess[estpars] <- rlnorm(
      n=length(estpars),
      meanlog=log(theta.guess[estpars]),
      sdlog=1
      )
    m2 <- mif2(
      m1,			#line that feeds the pomp object (i.e. the actual process and observation models)
      Nmif=10,			#Nmif controls how many number of filtering iterations to perform. Originially set to 50.
      start=theta.guess,
      transform=TRUE,
      rw.sd=rw.sd(R0=0.02,mu=0.02,sigma=0.02,gamma=0.01,rho=0.002,psi=0.02,S_0=0.02,E_0=0.02,I_0=0.02,R_0=0.02),  #dictating variance around proposals to search for each param. very important for tuning future code
      cooling.fraction.50=0.95,
      Np=200			#Np controls the number of particles used in filtering.  Default set to 2000
      )
    m2 <- continue(m2,Nmif=50,cooling.fraction=0.8)	#the function "continue" allows one to add iterations to a chain 
    m2 <- continue(m2,Nmif=50,cooling.fraction=0.6)	#in the process, we perform 3 more sets of 50 steps
    m2 <- continue(m2,Nmif=50,cooling.fraction=0.2)	#for each set, we are decreasing the cooling.fraction (the manner in which the intensity of  the  parameter  perturbations  is  reduced  with  successive  filtering  iterations.)
							#reducing the cooling.fraction reduces the perterbations made to the parameters in each iteration
    ll <- replicate(n=5,logLik(pfilter(m2,Np=1000)))		#Caluclating likelihood (dafault was 10000), but I'm a little confused by this step.  Why use pfilter? M2 contains the results from the previous iterations, so it must just be extrating something
    list(mif=m2,ll=ll)
    }

## ----gompertz-post-mif2--------#calculating final stats for the lhood runs
loglik.true <- replicate(n=5,logLik(pfilter(m1,Np=1000,params=params_true)))   #Np dafault was 10,000. extracting the "true" loglikelhood, but I'm not exactly sure how the replicate function is doing this...  It might be repeating the whole optimization processes anew, but that seems inefficient
loglik.true <- logmeanexp(loglik.true,se=TRUE)
theta.mif <- t(sapply(mf,function(x)coef(x$mif)))	#extracting the best lhood value from the mf object we created above
loglik.mif <- t(sapply(mf,function(x)logmeanexp(x$ll,se=TRUE)))
best <- which.max(loglik.mif[,1])
theta.mif <- theta.mif[best,]
loglik.mif <- loglik.mif[best,]
rbind(
  mle=c(signif(theta.mif[estpars],10),loglik=round(loglik.mif,2)),
  truth=c(signif(params_true[estpars],10),loglik=round(loglik.true,2))
  ) -> results.table

## ----mif2-plot,echo=FALSE,cache=FALSE,fig.height=6-------#plotting the results of the 10 parallel chains
op <- par(mfrow=c(4,3),mar=c(3,3,0,0),mgp=c(2,1,0),bty='l')
loglik <- sapply(mf,function(x)conv.rec(x$mif,"loglik"))
log.R0 <- sapply(mf,function(x)conv.rec(x$mif,"R0"))
log.mu <- sapply(mf,function(x)conv.rec(x$mif,"mu"))
log.sigma <- sapply(mf,function(x)conv.rec(x$mif,"sigma"))
log.gamma <- sapply(mf,function(x)conv.rec(x$mif,"gamma"))
log.rho <- sapply(mf,function(x)conv.rec(x$mif,"rho"))
log.psi <- sapply(mf,function(x)conv.rec(x$mif,"psi"))
log.S_0 <- sapply(mf,function(x)conv.rec(x$mif,"S_0"))
log.E_0 <- sapply(mf,function(x)conv.rec(x$mif,"E_0"))
log.I_0 <- sapply(mf,function(x)conv.rec(x$mif,"I_0"))
log.R_0 <- sapply(mf,function(x)conv.rec(x$mif,"R_0"))

matplot(loglik,type='l',lty=1,xlab="",ylab=expression(log~L),xaxt='n',ylim=max(loglik,na.rm=T)+c(-12,3))
matplot(log.R0,type='l',lty=1,xlab="",ylab=expression(log~R0),xaxt='n')
matplot(log.mu,type='l',lty=1,xlab="",ylab=expression(log~mu),xaxt='n')
matplot(log.sigma,type='l',lty=1,xlab="",ylab=expression(log~sigma),xaxt='n')
matplot(log.gamma,type='l',lty=1,xlab="",ylab=expression(log~gamma),xaxt='n')
matplot(log.rho,type='l',lty=1,xlab="",ylab=expression(log~rho),xaxt='n')
matplot(log.psi,type='l',lty=1,xlab="",ylab=expression(log~psi),xaxt='n')
matplot(log.S_0,type='l',lty=1,xlab="",ylab=expression(log~S_0),xaxt='n')
matplot(log.E_0,type='l',lty=1,xlab="",ylab=expression(log~E_0),xaxt='n')
matplot(log.I_0,type='l',lty=1,xlab="",ylab=expression(log~I_0),xaxt='n')
matplot(log.R_0,type='l',lty=1,xlab="",ylab=expression(log~R_0),xaxt='n')
par(op)

## ----first-mif-results-table,echo=FALSE,cache=FALSE----------------------
print(results.table)



